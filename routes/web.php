<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/register', 'PagesController@registershow');
Route::get('/login','PagesController@loginshow');
Route::post('/register/form',['uses'=>'PagesController@register']);
Route::post('/login/form',['uses'=>'PagesController@login']);

