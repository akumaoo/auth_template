@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header center"><strong>{{ __('Register') }}</strong></div>

                <div class="card-body" style="margin-top:30px">
                    <form id="form-register">
                        @csrf

                        <div class="form-group row">
                            <label for="Username" class="col-md-4 col-form-label text-md-right">{{ __('Username:') }}</label>

                            <div class="col-md-6">
                                <input id="Username" type="text" class="form-control{{ $errors->has('Username') ? ' is-invalid' : '' }}" name="Username" value="{{ old('Username') }}" required autofocus>

                                @if ($errors->has('Username'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('Username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row collapse" id="admin-acc-row">
                                <label for="Admin-acc" class="col-md-4 col-form-label text-md-right">{{ __('Admin Access Key') }}</label>
    
                                <div class="col-md-6">
                                    <input id="Admin-acc" type="password" class="form-control{{ $errors->has('Admin-acc') ? ' is-invalid' : '' }}" name="Admin-acc">
                                    <span class="invalid-feedback" id="invalidacckey" role="alert">
                                        <strong>{{ $errors->first('Admin-acc') }}</strong>
                                    </span>
                                </div>
                        </div>

                        <div class="form-group row mb-4">
                            <div class="col-md-6 offset-md-4" style="margin-left:70%;">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
