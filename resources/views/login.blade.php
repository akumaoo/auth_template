@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header center"><strong>{{ __('Login') }}</strong></div>
                @include('Includes.messages')
                <div class="card-body" style="margin-top:30px">
                    <form id='form-login'>
                        @csrf

                        <div class="form-group row">
                            <label for="Usermail" class="col-md-4 col-form-label text-md-right">{{ __('Username or E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="Usermail" type="text" class="form-control{{ $errors->has('error') ? ' is-invalid' : '' }}" name="Usermail" value="{{ old('Usermail') }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <div class="col-md-8 offset-md-8" style="margin-left:71%;">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                               
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
