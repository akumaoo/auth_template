@if(count($errors)>0)
	@foreach($errors-> all() as $error)
		<div class='alert alert-danger col-lg-12'>
			<div class="custom-msg">
				{{$error}}
			</div>
		</div>
	@endforeach
@endif

@if(session('success'))
	<div class="alert alert-success col-lg-12">
		<div class="custom-msg">
			{{session('success')}}
		</div>
	</div>
@endif

@if(session('error'))
	<div class="alert alert-danger col-lg-12">
		<div class="custom-msg">
			{{session('error')}}
		</div>
	</div>
@endif
