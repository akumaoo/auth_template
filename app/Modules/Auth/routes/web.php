<?php

Route::group(['module' => 'Auth', 'middleware' => ['web','guest'], 'namespace' => 'App\Modules\Auth\Controllers'], function() {

    Route::resource('Auth', 'AuthController');

});
