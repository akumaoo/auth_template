<?php

namespace App\Modules\Auth\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Auth\Models\Auth;
use Validator;

class AuthController extends Controller
{



    public function update(Request $request, $id)
    {
        //
    }

    public function register(Request $request)
    {
        try
        {
            $validator = Validator::make($request->all(), [
                'username'      =>  'required|max:20|unique:auths,username',
                'email'         =>  'required|email|max:30|unique:auths,email',
                'password'      =>  'required|max:200',
            ]);

            if ($validator->fails()) {
                $message = $validator->errors();
                return getAPIResponse('fail', $message);
            }else{
                return Auth::register($request);
            }
        }catch(Exception $e)
        {
            return generalErrorResponse($e->getMessage());
        }
    }

    public function login(Request $request)
    {
        try
        {
            if ($request->username==null||$request->email==null||$request->password==null) {
                $message = $validator->errors();
                return getAPIResponse('fail', $message);
            }else{
                return Auth::login($request);
            }
        }catch(Exception $e)
        {
            return generalErrorResponse($e->getMessage());
        }
    }
}
