<?php

namespace App\Modules\Auth\Models;

use Illuminate\Database\Eloquent\Model;

class Auth extends Model {

    protected $table='auths';
    
    public static function register($data){
        try{
            $auth=new self;
            $auth->username=$data->username;
            $auth->email=$data->email;
            $auth->password=bcrypt($data->password);

            $auth->save();

            return getAPIResponse('success',responseMessage('create'));

        }catch(Exception $e)
        {
            return generalErrorResponse($e->getMessage());
        }
    }
 

    public static function login($data){
        try{

            $userdata=self::where('username',$data->username)
                            ->orWhere('email',$data->email)->get();

            if(count($userdata)>0)
            {
                if(bcrypt($data->password)==$userdata->first()->password)
                {
                    return getAPIResponse('success',responseMessage('success-req'));
                }
                else
                {
                    return getAPIResponse('fail',responseMessage('x-pass'));
                }
            }
            else
            {
                return getAPIResponse('fail',responseMessage('not-found'));
            }


            return getAPIResponse('success',responseMessage('create'));
            
        }catch(Exception $e)
        {
            return generalErrorResponse($e->getMessage());
        }
    }

}
