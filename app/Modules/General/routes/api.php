<?php

Route::group(['module' => 'General', 'middleware' => ['api'], 'namespace' => 'App\Modules\General\Controllers', 'prefix' => 'general'], function() {

    Route::resource('General', 'GeneralController');
    Route::post('/login', ['uses' => 'GeneralController@userLogin','as'=>'login']);
});
