<?php
/**
 *	General Helper  
 */
function getAPIResponse($type, $message=null, $data=null){
	if ($type == 'success') {	
		$status = ['status_code' => 0];
	}else if($type == 'fail'){
		$status = ['status_code' => 1];
	}
	return ['status' => $status['status_code'],'message' => $message,'data' => $data];
}

function generalErrorResponse($errorMessage) {
    return '{"status_code": 999, "status": "fail", "message": "' . $errorMessage . '"}';
}
function responseMessage($type){
	if($type=='create'){
		return 'Record has been added.';
	}
	else if($type=='update'){
		return 'Record updated.';
	}
	else if($type=='delete'){
		return 'Record deleted.';
	}
	else if($type=='not-found'){
		return 'Record not found.';
	}
	else if($type=='email-exist')
	{
		return 'Email has already been taken.';
	}
	else if($type=='success-req')
	{
		return 'Success request';
	}
	else if($type=='x-pass')
	{
		return 'Incorrect Password';
	}
}