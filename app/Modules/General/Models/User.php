<?php

namespace App\Modules\General\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {

    //
    protected $table = 'users';
    protected $guard = 'email';

    public static function userLogin($request){
    	$password = hash('md5', $request['password']);
    	return self::where('email', $request['email'])->where('password', $password)->exists();
    }

    public static function addUser($credentials){
        try {
            if (User::where('email', $credentials['email'])->exists()) {
                return getAPIResponse('fail', 'email exists');
            }else{
              self::insert($credentials); 
            }
        } catch (Exception $e) {
            return generalErrorResponse($e->getMessage()); 
        }
    }
}