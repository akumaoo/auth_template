<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function register(Request $request)
    {
       return 'register method';
    }

    public function login(Request $request)
    {
       return 'login method';
    }
    public function registershow()
    {
       return view('/register');
    }
    public function loginshow()
    {
        return view('/login');
    }
}
